How to run FitNext
==================

1. Install redis server

```
$ brew install redis
```

or

```
$ sudo apt-get install redis
```

2. Create virtualenv directory within root directory

```
$ mkdir virtualenv
```

3. Create a new apps within virtualenv directory

```
$ virtualenv virtualenv/fitnext
```

4. Activate virtualenv

```
$ cd virtualenv/fitnext/bin
$ source activate
```

5. After virtualenv is activated install the prerequisite for this project

```
(fitnext) $ pip install -r requirements.txt
```

6. Open first console to run redis server

```
$ redis-server
```

7. Open second console to run FitNext

```
(fitnext) $ cd fitnext
(fitnext) $ ./manage.py runserver 0.0.0.0:8000
```

7. Open second console to run Celery

```
(fitnext) $ celery -A fitnext.celery:app worker -c 4  -B --loglevel=INFO
```

8. Test each endpoint by importing fitnext.postman_collection.json to your Postman

```
POST /challenge/run
POST /challenge/run_db
POST /challenge/run_queue
```

There are three types of the execution:

- First, you could read the challenge code using just if without database. 
- Second, you could read the challenge code that manage by the DB. Then execute the code but that could be take long time if within the challenge code use a lot of I/O operation.
- Third, you could read the challenge code that manage by the DB and run it within the celery tasks. That things is done by Celery because somehow database operation for reading sensor data or any datasource could be take long time until the data is prepared and consumed by the challenge code. So its not wise if the challenge execution is exhausting the main server. So the challenge code is executed in separate server as a worker server.
