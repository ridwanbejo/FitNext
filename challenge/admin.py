# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Challenge, ChallengeLog, SensorDataset

# Register your models here.

class ChallengeAdmin (admin.ModelAdmin):
    list_display = ['name', 'code', 'created_at']
    search_fields = ['name', 'code']
    list_per_page = 25

admin.site.register(Challenge, ChallengeAdmin)

class ChallengeLogAdmin (admin.ModelAdmin):
    list_display = ['user', 'challenge', 'result', 'created_at']
    list_filter = ('challenge',)
    list_per_page = 25

admin.site.register(ChallengeLog, ChallengeLogAdmin)

class SensorDatasetAdmin (admin.ModelAdmin):
    list_display = ['user', 'sensor_types', 'sensor_values', 'logged_at', 'created_at']
    list_filter = ('sensor_types',)
    list_per_page = 25

admin.site.register(SensorDataset, SensorDatasetAdmin)