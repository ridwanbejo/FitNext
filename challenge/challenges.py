from .models import SensorDataset
from django.contrib.auth.models import User

def myFirstChallenge(x=0, y=0):

    """
    sensor types: None
    state: true, false
    payload: {
        "challengeId": 1,
        "arguments": {
            "x": 10,
            "y": 100
        }
    }
    """

    if (x == y):
        return True
    return False

def mySecondChallenge(x=0, y=0):

    """
    sensor types: None
    state: true, false
    payload: {
        "challengeId": 1,
        "arguments": {
            "x": 10,
            "y": 100
        }
    }
    """

    if ((x + y) > 1):
        return True
    return False

def beActiveChallenge(userId=None, logged_at=None):
    
    """
    sensor types: activity log
    state: overactive, active, passive, overpassive
    payload: {
        "challengeId": 3,
        "arguments": {
            "userId": 1,
            "logged_at": "2018-06-21"
        }
    }
    """

    user = User.objects.get(id=userId)
    activities_per_day = SensorDataset.objects.filter(user=user, sensor_types="activity_log", logged_at__date=logged_at)

    activities_per_day_count = activities_per_day.count()

    if activities_per_day_count > 10:
        return 'overactive'
    elif activities_per_day_count >= 5 and activities_per_day_count <= 10:
        return 'active'
    elif activities_per_day_count > 0  and activities_per_day_count < 5:
        return 'passive'
    elif activities_per_day_count <= 0:
        return 'overpassive'
    
def footStepChallenge():

    # footstep
    # state: active, passive

    return 'active'

def sleepTimeChallenge():
    
    # sleep time
    # state: normal, less sleep, over sleep

    return 'normal'

def sleepBedTimeChallenge():
    
    # sleep bed time
    # state: normal, too early, too late

    return 'normal'

def sleepWakeUpTimeChallenge():
    
    # sleep wakeup time
    # state: normal, too early, too late

    return 'normal'

def bloodPressureChallenge():
    
    # bloodPressure
    # state: normal, high, low

    return 'normal'

def smokingChallenge():
    
    # smoking
    # state: active, passive, contaminated

    return 'passive'
