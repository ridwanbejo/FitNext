# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-06-21 10:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('challenge', '0004_auto_20180621_1030'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sensordataset',
            name='sensor_types',
            field=models.CharField(choices=[('footstep', 'Step Counter'), ('sleep_duration', 'Sleep Duration'), ('sleep_bed_time', 'Sleep Bed Time'), ('sleep_wakeup_time', 'Sleep Wake Up Time'), ('location', 'GPS Location'), ('blood_presure', 'Blood Pressure')], max_length=50),
        ),
    ]
