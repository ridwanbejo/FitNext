# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Challenge(models.Model):
    name = models.CharField(max_length=20)
    code = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.name

class ChallengeLog(models.Model):
    user = models.ForeignKey(User, related_name='challenge_log', null=True)
    challenge = models.ForeignKey(Challenge)
    payload = models.TextField()
    result = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.challenge.name

class SensorDataset(models.Model):
    SENSOR_TYPES_CHOICES = (
        ('footstep', 'Step Counter'),
        ('sleep_duration', 'Sleep Duration'),
        ('sleep_bed_time', 'Sleep Bed Time'),
        ('sleep_wakeup_time', 'Sleep Wake Up Time'),
        ('location', 'GPS Location'),
        ('blood_presure', 'Blood Pressure'),
        ('smoking_time', 'Smoking Time'),
        ('activity_log', 'Activity Log'),
        ('mood_log', 'Mood Log')
    )

    user = models.ForeignKey(User, related_name='sensor_dataset', null=True)
    sensor_values = models.CharField(max_length=255)
    sensor_types = models.CharField(max_length=50, choices=SENSOR_TYPES_CHOICES)
    logged_at = models.DateTimeField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.sensor_types + ' ' + self.sensor_values
    