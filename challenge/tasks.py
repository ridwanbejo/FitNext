from __future__ import absolute_import

from celery import shared_task
from challenge.models import Challenge, ChallengeLog
from django.contrib.auth.models import User

import importlib
            
@shared_task
def run_challenge(payload):
    challenge = Challenge.objects.get(id=payload['challengeId'])
    function_string = challenge.code
    print function_string

    mod_name, func_name = function_string.rsplit('.',1)
    
    mod = importlib.import_module(mod_name)
    func = getattr(mod, func_name)
        
    result = func(**payload['arguments'])

    """
    Store the result to DB
    """

    log = ChallengeLog(
        challenge=challenge,
        payload=payload,
        result=result
    )
    
    print payload['arguments']

    if 'userId' in payload['arguments']:
        userId = payload['arguments']['userId']
        log.user = User.objects.get(id=userId)

    log.save()

    """
    code for send the update (websocket / pubsub / silent notification) is here ...
    """ 

    # send_challenge_result_to_user_device()

    return result