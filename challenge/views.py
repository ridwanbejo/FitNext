# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

import json
import importlib
            
from .models import Challenge
from challenge.tasks import run_challenge

def firstChallenge(x=0, y=0):
    if (x == y):
        return True
    return False

def secondChallenge(x=0, y=0):
    if ((x + y) > 1):
        return True
    return False

# Create your views here.

@csrf_exempt
def run_challenge_plain(request):
    if request.body:
            payload = json.loads(request.body)

            func = ''
            if payload['challengeId'] == 1:
                func = 'firstChallenge'
            elif payload['challengeId'] == 2:
                func = 'secondChallenge'

            result = globals()[func](**payload['arguments'])

            return JsonResponse({'message': 'Running challenge success', 'payload': payload, 'result': result })
    else:
        return JsonResponse({'message':'Request unavailable!'})

@csrf_exempt
def run_challenge_db(request):
    if request.body:
            payload = json.loads(request.body)

            func = Challenge.objects.get(id=payload['challengeId'])
            print func

            function_string = func.code
            print function_string

            mod_name, func_name = function_string.rsplit('.',1)
            
            mod = importlib.import_module(mod_name)
            func = getattr(mod, func_name)
            
            result = func(**payload['arguments'])

            print result

            return JsonResponse({'message': 'Running challenge success', 'payload': payload, 'result': result })
    else:
        return JsonResponse({'message':'Request unavailable!'})

@csrf_exempt
def run_challenge_queue(request):
    if request.body:
            payload = json.loads(request.body)

            run_challenge.delay(payload)

            return JsonResponse({'message': 'Running challenge on queue is success. You have to wait or subscribe for the result.', 'payload': payload })
    else:
        return JsonResponse({'message':'Request unavailable!'})