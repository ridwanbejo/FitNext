"""

$ locust --host=http://localhost:8000 -f locustfile.py

"""
from locust import HttpLocust, TaskSet, task
import json

class MyTaskSet(TaskSet):
    @task
    def index(self):
        payload = {
            "challengeId": 2,
            "arguments": {
                "x": 100,
                "y": 100
            }
        }

        self.client.post("/challenge/run_queue", data=json.dumps(payload), headers={"content-type":"application/json"}, )

class MyLocust(HttpLocust):
    task_set = MyTaskSet
    min_wait = 5000
    max_wait = 15000